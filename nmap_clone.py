import optparse
import socket
from socket import *
from threading import *
screenLock = Semaphore(value=1)


def connScan(tgtHost, tgtPort):
    try:
        connSkt = socket(AF_INET, SOCK_STREAM)
        connSkt.connect((tgtHost, tgtPort))
       # connSkt.send("ViolentPython\n")
        results = connSkt.recv(100)
        screenLock.acquire()
        print("[+] ", tgtPort, " /tcp open")
        print("[+] " + str(results))
    except:
        screenLock.acquire()
        print("[-] ", tgtPort, " /tcp closed")
    finally:
        screenLock.release()
        connSkt.close()



def portScan(tgtHost, tgtPorts):
    try:
        tgtIP = gethostbyname(tgtHost)
    except:
        print("[-] Cannot resolve " + tgtHost + ": Unknown host")
        return
    try:
        tgtName = gethostbyaddr(tgtIP)
        print("\n[+] Scan results for: " + tgtName[0])
        
    except:
        print("\n Scan results for: " + tgtIP)
    setdefaulttimeout(1)
    for tgtPort in tgtPorts:
        t = Thread(target = connScan, args=(tgtHost, int(tgtPort)))
        t.start()


def main():
    global tgtPorts
    parser = optparse.OptionParser('usage %prog -H <target host> -p <target port>')
    parser.add_option('-H', dest='tgtHost', type='string', help='specify target host')
    parser.add_option('-p', dest='tgtPort', type='string', help='specify target port')
    (options, args) = parser.parse_args()
    tgtHost = options.tgtHost
    tgtPorts = str(options.tgtPort).split(", ")
    if (tgtHost == None) | (tgtPorts[0] == None):
        print(parser.usage)
        exit(0)
    portScan(tgtHost, tgtPorts)
main()



